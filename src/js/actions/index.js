import {ADD_POST, DEL_POST} from "../constants/actionTypes";

export const addPost = (newPost) => {
	return {
		type: ADD_POST,
		newPost
	}
};

export const delPost = (post) => {
	return {
		type: DEL_POST,
		post
	}
};