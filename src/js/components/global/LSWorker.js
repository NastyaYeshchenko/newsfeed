const postsSection = "iFeedVideos";

export default class LSWorker {
	static addNewPost(newPost) {
		let posts = this.getPosts() || [];
		posts.push(newPost);

		localStorage.setItem(postsSection, JSON.stringify(posts));
	}
	
	static deletePost(postTime) {
		let posts = this.getPosts() || [];
		let delPost = posts.find(function(post) {
			return Math.floor(Date.parse(postTime)/1000) == Math.floor(post.postTime/1000);
		});
		posts.splice(posts.indexOf(delPost), 1);
		
		localStorage.setItem(postsSection, JSON.stringify(posts));
	}

	static getPosts() {
		/* to remove */
		if (!localStorage.getItem(postsSection)) {
			localStorage.setItem(postsSection, this.initMockData());
		}

		return JSON.parse(localStorage.getItem(postsSection));
	}

	static initMockData() {
		let videosMock = [{
			description: "David Guetta - Play Hard ft. Ne-Yo, Akon (Official Video)",
			link: "https://www.youtube.com/watch?v=5dbEhBKGOtY",
			postTime: 1521820281000
		}, {
			description: "LITTLE BIG – SKIBIDI (official music video)",
			link: "https://www.youtube.com/watch?v=mDFBTdToRmw",
			postTime: 1521388281000
		}];

		return JSON.stringify(videosMock);
	}
}