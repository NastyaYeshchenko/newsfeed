import React from "react";
import LSWorker from "./global/LSWorker";
import {delPost} from "../actions/index";
import Button from "./shared/button";
import {connect} from "react-redux";

class VideoFooter extends React.Component {
	constructor(props) {
		super(props);

		this.delPost = this.delPost.bind(this);
	}
	
	delPost() {
		LSWorker.deletePost(this.props.postTime);
		this.props.delPost(this.props.postTime);
	}

	render() {
		return (
			<footer className="video-footer">
				<section className="left-video-description">
					Posted: {this.props.postTime}
				</section>
				<button className="btnDel" onClick={this.delPost}><img src="src/assets/img/delete.png"/></button>
			</footer>
		)
	}
}

const mapDispatchToProps = (dispatch) => {
	return {
		delPost: function (postTime) {
			return dispatch(delPost(postTime));
		}
	}
};

VideoFooter = connect(null, mapDispatchToProps)(VideoFooter);

export {VideoFooter};