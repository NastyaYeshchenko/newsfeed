import {ADD_POST, DEL_POST} from "../constants/actionTypes";
import LSWorker from "../components/global/LSWorker";

const initialState = {
	posts: LSWorker.getPosts()
};

const newsFeedApp = (state = initialState, action) => {
	switch (action.type) {
		case ADD_POST: {
			return Object.assign({}, state, {
				posts: [...state.posts, action.newPost]
			});
		}
		case DEL_POST: {
			let posts = state.posts;
			let delPost = posts.find(function(post) {
				return Math.floor(Date.parse(action.post)/1000) == Math.floor(post.postTime/1000);
			});
			posts.splice(posts.indexOf(delPost), 1);
			return Object.assign({}, state, {
				posts: [...posts]
			});
		}
		default: {
			return state
		}
	}
};

export default newsFeedApp;