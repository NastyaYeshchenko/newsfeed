const path = require('path');

module.exports = {
	devtool: 'source-map',
	entry: "./src/js/index.jsx",
	output: {
		path: path.join(__dirname, 'build'),
		filename: 'bundle.js',
		publicPath: '/build/'
	},
	module: {
		rules: [
			{
				test: /.jsx?$/,
				loader: "babel-loader",
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: [".js", ".jsx"]
	}
};